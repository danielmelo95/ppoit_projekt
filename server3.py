from threading import Lock, Thread
from flask import Flask, render_template
from flask_socketio import SocketIO, emit, disconnect
import time
import random
import RPi.GPIO as GPIO
import serial
import eventlet

eventlet.monkey_patch()

async_mode = None

app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret!'
#socketio = SocketIO(app, async_mode=async_mode)
socketio = SocketIO(app, async_mode='eventlet')


### INFO
# pred svetelny senzor treba 10 kOhm odpor
#
###



global speeds
speeds = []

# Funkcie
global tempomat
global lane_assistant
global svetla
tempomat = False
lane_assistant = False
svetla = False

# PINs
pin_forward = 16        # afticko dopredu
pin_backward = 18       # afticko dozadu
pin_engine = 32         # motor
pin_steer = 33          # natocenie kolesa
pin_photo_in = 15       # fotosensor
# pin_ultra =           # ultrazvuk
pin_ir_left = 13        # infra sensor lavy
pin_ir_right = 11       # infra sensor pravy
pin_led = 22            # led zasvietenie



# variables
steer = 0
speed = 0
steer_right_min = 5
steer_left_max = 25

ser=serial.Serial("/dev/ttyS0",9600)
ser.baudrate=9600

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)

@socketio.on("start")
def start():
    print("start")
    setup()
    #loop()

@socketio.on("stop")
def stop():
    destroy()

@socketio.on("speed_tilt")
def data(data):
    global pwm,pwmSteer,steer,speed,tempomat
    pwm.ChangeDutyCycle(float(data.get("speed")))
    pwmSteer.ChangeDutyCycle(float(data.get("tilt")))
    steer = data.get("tilt")
    speed = data.get("speed")
    if data.get("tempomat") == 0:
        tempomat = False
    else:
        tempomat = True
    if data.get("line_assistant") == 0:
        lane_assistant = False
    else:
        lane_assistant = True
   
def sendData():
    global  speed, steer
    msg = str(speed) + ";" + str(steer)
    socketio.emit('data',msg)

def setup():
    global sp, pwm, pwmSteer

    GPIO.setmode(GPIO.BOARD)
    # GPIO setup
    GPIO.setwarnings(True)

    # pin setup
    GPIO.setup(pin_forward,     GPIO.OUT)
    GPIO.setup(pin_backward,    GPIO.OUT)
    GPIO.setup(pin_engine,      GPIO.OUT)
    GPIO.setup(pin_steer,       GPIO.OUT)
    GPIO.setup(pin_photo_in,    GPIO.IN)
    GPIO.setup(pin_ir_left,     GPIO.IN, pull_up_down = GPIO.PUD_UP)
    GPIO.setup(pin_ir_right,    GPIO.IN, pull_up_down = GPIO.PUD_UP)
    GPIO.setup(pin_led,         GPIO.OUT)


    # Zaradenie 1
    accelerate()
    
    pwm = GPIO.PWM(pin_engine, 100)
    pwm.start(0)

    pwmSteer = GPIO.PWM(pin_steer, 100)
    pwmSteer.start(0)
    
    thread = Thread(target=loop)
    thread.start()

def destroy():
    global pwm, pwmSteer
    pwm.stop()                         # stop PWM
    pwmSteer.stop()                     # stop PWM
    GPIO.cleanup()

def accelerate():
    GPIO.output(pin_forward,    GPIO.HIGH)
    GPIO.output(pin_backward,   GPIO.LOW)

def stay():
    GPIO.output(pin_backward,   GPIO.LOW)
    GPIO.output(pin_forward,    GPIO.LOW)

# functions
def do_steer():
    pass

def get_distance():
    global ser
    ser.write(bytes([0x55]))
    data = ser.read(2)       
    if len(data) != 2:
        raise RuntimeError("No distance value received.")
    distance = (data[1] + data[0] << 8) / 10
    return distance

def sum(arr):
    _sum = 0
    for i in range(0, len(arr)):
        _sum+=arr[i]
    return _sum/len(arr)

def autodrive(dist):
    global pwm,speeds,speed
    speeds.append(dist)
    if len(speeds) >= 5:
        if sum(speeds) > 3000:
            speed = 100
        else:
            speed = 0
        pwm.ChangeDutyCycle(speed)
        speeds = []

def turn():
    global wheelRotation, steer
    wr = wheelRotation
    if (GPIO.input(pin_ir_right)):    # If right color is black 
        wr = wheelRotation-5             # then turn right
    if (wheelRotation < steer_right_min):    # Limit of wheelRotation
        wr = steer_right_min
    if (GPIO.input(pin_ir_left)):    # If left color is black 
        wr = wheelRotation+5       # then turn left
    if (wheelRotation > steer_left_max):    # Limit of wheelRotation
        wr = steer_left_max
    wheelRotation = wr
    steer = wheelRotation
    return wr

def loop():
    global pwm,tempomat,lane_assistant,svetla,pin_led,pin_photo_in
    while True:
        sendData()
        if tempomat:
            autodrive(3001)
            #autodrive(get_distance()) #ser.read() cries and wont work
            #time.sleep(0.001)
            
	if lane_assistant:
	    pwmSteer.ChangeDutyCycle(turn())

        if svetla:
            dark = GPIO.input(pin_photo_in)
            print(dark)
            if dark:
                GPIO.output(pin_led,True)
            else:
                GPIO.output(pin_led,False)
        time.sleep(.1)
                
if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0", port=80, debug=True)