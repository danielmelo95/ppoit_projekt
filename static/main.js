
const SERVER_ADRESS = "127.0.0.1";
const PORT = 80;

socket = io();

function start() {
    document.getElementById("start_btn").disabled = true;
    document.getElementById("stop_btn").disabled = false;

    var arr = [].slice.call(document.getElementsByClassName('slider'));
    console.log(arr);
    arr.forEach(element => {
        element.disabled = false;
        element.style.cursor = 'default';
    });

    socket.emit("start");
}

function stop() {
    document.getElementById("start_btn").disabled = false;
    document.getElementById("stop_btn").disabled = true;
    var arr = [].slice.call(document.getElementsByClassName('slider'));
    console.log(arr);
    arr.forEach(element => {
        element.disabled = true;
        element.style.cursor = 'not-allowed';

    });


    socket.emit("stop");
}

function changeParameters(tilt, speed, temp, line_ass){
    console.log(tilt + "---" + speed+"---"+temp+"---"+line_ass);
    var obj = {
        "speed" : speed, 
        "tilt" : tilt,
        "tempomat": temp, 
        "line_assistant": line_ass
    }
    console.log(socket);
    socket.emit("speed_tilt", obj);

}
function speedChange(speed) {
    socket.emit("speed", speed);
}

function turnChange(turn){
    socket.emit("turn",turn);
}

socket.on('data', function(message) {
    console.log("prijimame: ")
    
    console.log(message); 
    var values = message.split(";");
    var speed = values[0]; 
    var steer = values[1];
    speed = parseInt(speed);
    steer = parseInt(steer);

    redrawChart(steer, speed);

    });

socket.on('connect', () => {
    console.log("Mame kontakt");
});

